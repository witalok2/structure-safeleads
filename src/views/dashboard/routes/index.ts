export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/index.vue'),
    meta: {requiresAuth: true}
  },
  {
    path: '/projects',
    name: 'projects',
    component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/projects.vue'),
    meta: {requiresAuth: true}
  }
]